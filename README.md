# **RNN for Gradient-Spoiled Sequences:**

This is a sample code for "Fast and Accurate Modeling of Transient-state Gradient-Spoiled Sequences by Recurrent Neural Networks." arXiv preprint arXiv:2008.07440 (2020).

Copyright: UMC Utrecht,2020.

Hannah Liu. For any questions, please feel free to contact: hliu@umcutrecht.nl  

## **Usage:**

1) Download the data from surfdrive: https://surfdrive.surf.nl/files/index.php/s/NmczfIz7qIAe39U

2) The trained RNN network is saved as /Experiments/Results/RNN_EPG.hdf5 in the surfdrive folder. 

3) Put the data and code in the same parent folder, and run rnn_training_and_validation.py, this code runs the training and generate a RNN-computed dictionary.

