
"""RNN_Training_and_Validation.py
# 0) Loading Packages
"""

# Commented out IPython magic to ensure Python compatibility.
from __future__ import absolute_import, division, print_function, unicode_literals
try:
  # %tensorflow_version only exists in Colab.
#   %tensorflow_version 2.x
except Exception:
  pass

# tensorflow related
import tensorflow as tf
from tensorflow.python.ops import array_ops
from tensorflow.keras import backend as K
import tensorflow.keras as keras

# general packages
import scipy.io as sio     ## read matlab file data
import h5py
import time                ## time performance test
import numpy as np
import matplotlib.pyplot as plt
import os
import random
import datetime
import timeit

# gpu detection
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
  raise SystemError('GPU device not found')
print('Found GPU at: {}'.format(device_name))

# validate tf version
print(tf.__version__)
!nvidia-smi

"""# 1) Define RNN layer with 3 stacked GRU units"""

def build_model_GRU_init(rnn_units, input_batch, input_size, output_size):

  inputs = tf.keras.Input(batch_size = input_batch, shape=[None, input_size],name='input_1');
  init_state = tf.keras.Input(batch_size = input_batch, shape = [3], name='input_2');
  h0 = tf.keras.layers.Dense(rnn_units,activation="linear")(init_state)

  gru1 = tf.keras.layers.GRU(rnn_units,
              return_sequences=True,
              stateful=False, ## See definition of the stateful input, 
              recurrent_initializer='glorot_uniform',unroll=False);
  gru2 = tf.keras.layers.GRU(rnn_units,
              return_sequences=True,
              stateful=False, ## See definition of the stateful input, 
              recurrent_initializer='glorot_uniform',unroll=False);
  gru3 = tf.keras.layers.GRU(rnn_units,
              return_sequences=True,
              stateful=False, ## See definition of the stateful input, 
              recurrent_initializer='glorot_uniform',unroll=False);

  x1 = gru1(inputs, initial_state=h0);
  x2 = gru2(x1);
  x3 = gru3(x2);
  x4 = tf.keras.layers.Dense(output_size,activation="linear")(x3)
  return tf.keras.Model(inputs = [inputs, init_state], outputs = x4, name='GRU')

"""# 2) Data preparation"""

# 1) Some initial settings
sess = tf.compat.v1.Session(
    config=tf.compat.v1.ConfigProto(intra_op_parallelism_threads=2,inter_op_parallelism_threads=2))
tf.compat.v1.keras.backend.set_session(sess)
tf.keras.backend.clear_session()


# parameters setup
nTrain = 1000 
nTotal = 1500 
nOffset = 0
nSeq_Length = 224 * 5 # inversion pulse as the first TR
nInput =  5   # theta, T1, T2, TE, TR
nOutput = 3   # m, dm/dT1, dm/dT2
nBatch = 200
nRNN_Unit = 32
nEpoch = 1500
nVal_BatchSize = 6  # Number of batches used for validation during training

# Initial
Inv = []
# In
RFtrain=[]
T1=[]
T2=[]
TE = []
TR = []
# Out
Dictionary = []

# 2) Load data
# PART ONE (INVERSE)
# data selection
filename = ["1Spline5Flex","2Spline11Flex","3SinSFlex","4PiecewiseFlex","5Spline11Flex10Noise"] 

for ii in range(len(filename)):
  # read file one
  str1 = "./Experiments/RNN_Train/"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str1)
  RFtrain.append(traindata.get("rf")) # flipangles

  # read file two
  str2 = "./Experiments/RNN_Train/Gauss/spgr_"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str2)
  temp0 = traindata.get("dictionary")
  # temp0 = np.swapaxes(temp0,0,2)
  Dictionary.append(temp0)

  TE.append(traindata.get("TEAll"))
  TR.append(traindata.get("TRAll"))

  T1T2B1 = traindata.get("T1T2B1")
  T1.append(T1T2B1[:,0])
  T2.append(T1T2B1[:,1])
  Inv.append(-180.0 * np.ones(shape=(1))) # Changed on May,13th

# 2) Load data
# PART TWO (NO INVERSE)
# data selection
# filename = ["1Spline5Flex" ,"2Spline11Flex","3SinSFlex","4PiecewiseFlex","5Spline11Flex10Noise"]
for ii in range(len(filename)):
  # read file one
  str1 = "./Experiments/RNN_Train/"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str1)
  RFtrain.append(traindata.get("rf")) # flipangles

  # read file two
  str2 = "./Experiments/RNN_Train/Gauss_NoInv/spgr_"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str2)
  temp0 = traindata.get("dictionary")
  # temp0 = np.swapaxes(temp0,0,2)
  Dictionary.append(temp0)

  TE.append(traindata.get("TEAll"))
  TR.append(traindata.get("TRAll"))

  T1T2B1 = traindata.get("T1T2B1")
  T1.append(T1T2B1[:,0])
  T2.append(T1T2B1[:,1])
  # Inv.append(0.0 * np.ones(shape=(1)))
  Inv.append(180.0 * np.ones(shape=(1))) # Changed on May13th,2020

print(T1T2B1.shape)
print(RFtrain[9].shape,Dictionary[9].shape,T1[9].shape,Inv[9].shape,TE[9].shape,TR[9].shape)

# 2) Load data
# PART THREE (INVERSE, FIX TETR)
# data selection
# filename =  ["1Spline5Flex" ,"2Spline11Flex","3SinSFlex","4PiecewiseFlex","5Spline11Flex10Noise"] # "5Spline11Flex10Noise"]
for ii in range(len(filename)):
  # read file one
  str1 = "./Experiments/RNN_Train/"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str1)
  RFtrain.append(traindata.get("rf")) # flipangles

  # read file two
  str2 = "./Experiments/RNN_Train_TETR/Gauss/spgr_"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str2)
  temp0 = traindata.get("dictionary")
  # temp0 = np.swapaxes(temp0,0,2)
  Dictionary.append(temp0)

  TE.append(traindata.get("TEAll"))
  TR.append(traindata.get("TRAll"))

  T1T2B1 = traindata.get("T1T2B1")
  T1.append(T1T2B1[:,0])
  T2.append(T1T2B1[:,1])
  Inv.append(-180.0 * np.ones(shape=(1))) # Changed on May,13th
  # Inv.append(180.0 * np.ones(shape=(1))) 

print(T1T2B1.shape)
print(RFtrain[0].shape,Dictionary[0].shape,T1[0].shape,Inv[0].shape,TE[0].shape,TR[0].shape)

# 2) Load data
# PART TWO (NO INVERSE)
# data selection
# filename = ["1Spline5Flex" ,"2Spline11Flex","3SinSFlex","4PiecewiseFlex","5Spline11Flex10Noise"]
for ii in range(len(filename)):
  # read file one
  str1 = "./Experiments/RNN_Train/"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str1)
  RFtrain.append(traindata.get("rf")) # flipangles

  # read file two
  str2 = "./Experiments/RNN_Train/Gauss_NoInv/spgr_"  + filename[ii] + ".mat"
  traindata = sio.loadmat(str2)
  temp0 = traindata.get("dictionary")
  # temp0 = np.swapaxes(temp0,0,2)
  Dictionary.append(temp0)

  TE.append(traindata.get("TEAll"))
  TR.append(traindata.get("TRAll"))

  T1T2B1 = traindata.get("T1T2B1")
  T1.append(T1T2B1[:,0])
  T2.append(T1T2B1[:,1])
  # Inv.append(0.0 * np.ones(shape=(1)))
  Inv.append(180.0 * np.ones(shape=(1))) # Changed on May13th,2020

print(T1T2B1.shape)
print(RFtrain[9].shape,Dictionary[9].shape,T1[9].shape,Inv[9].shape,TE[9].shape,TR[9].shape)

# Summarize all the data!
# TWO: ADD inverse flag input
print(nInput)
type_rf = 20# 20 # len(filename)
Data = np.zeros(shape=(nTotal * type_rf, nSeq_Length, nInput),dtype="float32") # ndarray
Mag = np.zeros(shape=(nTotal * type_rf, nSeq_Length, 1),dtype="float32")
Deriv = np.zeros(shape=(nTotal * type_rf, nSeq_Length, 2),dtype="float32")
State_Input = np.zeros(shape = (nTotal * type_rf, 3), dtype="float32")
for ii in range(type_rf):
  temp0= RFtrain[ii]
  print(ii)
  temp1 = T1[ii]
  temp2 = T2[ii]
  temp1 = temp1[:,np.newaxis]
  temp2 = temp2[:,np.newaxis]

  temp3 = TE[ii]
  temp4 = TR[ii]

  Data[ii * nTotal:(ii+1) * nTotal,0:nSeq_Length,0] = temp0[nOffset:nTotal + nOffset,:] # RF
  Data[ii * nTotal:(ii+1) * nTotal,:,1] = temp1[nOffset:nTotal + nOffset]        # T1
  Data[ii * nTotal:(ii+1) * nTotal,:,2] = temp2[nOffset:nTotal + nOffset]        # T2
  Data[ii * nTotal:(ii+1) * nTotal,:,3] = temp3[nOffset:nTotal + nOffset,:]       # TE
  Data[ii * nTotal:(ii+1) * nTotal,:,4] = temp4[nOffset:nTotal + nOffset,:]       # TR


  State_Input[ii * nTotal:(ii+1) * nTotal,2] = Inv[ii]/180

  temp1 = Dictionary[ii]
  Mag[ii * nTotal:(ii+1) * nTotal,0:nSeq_Length,0] = (temp1[0,nOffset:nTotal + nOffset,0:nSeq_Length])
  Deriv[ii * nTotal:(ii+1) * nTotal,0:nSeq_Length,0] = (temp1[1,nOffset:nTotal + nOffset,0:nSeq_Length]) # T1
  Deriv[ii * nTotal:(ii+1) * nTotal,0:nSeq_Length,1] = (temp1[2,nOffset:nTotal + nOffset,0:nSeq_Length]) # T2

# Log scale
Deriv[:,:,0] = Deriv[:,:,0] * Data[:,:,1]
Deriv[:,:,1] = Deriv[:,:,1] * Data[:,:,2]

def generate_dataset(Data, State_Input, Mag, Deriv, sample_list):
  Data_use = Data[sample_list,:,:]
  Mag_use = Mag[sample_list,:,:]
  Deriv_use = Deriv[sample_list,:,:]
  State_Input_use = State_Input[sample_list,:]
  
  Mag_Full_use = np.concatenate((Mag_use,Deriv_use),axis=2)
  print(type_rf, nTrain, Data_use.shape,Mag_use.shape, State_Input_use.shape)

  dataset_use = tf.data.Dataset.from_tensor_slices(({"input_1": Data_use, "input_2": State_Input_use}, Mag_Full_use))
  dataset_use = dataset_use.batch(nBatch, drop_remainder=True) # nTotal
  dataset_use.shuffle(buffer_size=32, reshuffle_each_iteration=True)
  return dataset_use

# 4) Generate Dataset for training
shuffle = np.random.permutation(nTotal * type_rf)
# Training
Training_List = shuffle[0:nTrain * type_rf - nVal_BatchSize * nBatch]
dataset_train = generate_dataset(Data, State_Input, Mag, Deriv, Training_List)

# Validation
Validation_List = shuffle[nTrain * type_rf - nVal_BatchSize * nBatch:nTrain * type_rf]
dataset_valid = generate_dataset(Data, State_Input, Mag, Deriv, Validation_List)

# Test
Test_List = shuffle[nTrain * type_rf:nTotal * type_rf] # range(0,nTotal * type_rf) #
dataset_test = generate_dataset(Data, State_Input, Mag, Deriv, Test_List)


print(dataset_test)
print(dataset_test.take(2))

"""# 3) Generate the Network and Do the training"""

# 2) Regenerate the model with the maximum possible batch size
model = build_model_GRU_init(
    rnn_units = nRNN_Unit,
    input_batch = nBatch,
    input_size = nInput, # RF, T1, T2, don't put in mxy(n-1)
    output_size = nOutput) 
model.summary()

keras.utils.plot_model(model, show_shapes=True, to_file='Model.png')#

# 2) Do the training
str1 = "/content/drive/My Drive/RNN/Results/RNN_EPG.hdf5"


checkpoint = keras.callbacks.ModelCheckpoint(str1, monitor='loss', verbose=1, save_best_only=True, mode='min')
lr_callback =tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss',factor = 0.8, patience = 100,
      verbose = 1, mode = 'auto',min_delta = 0.0002, cooldown = 100, min_lr = 8e-5) # 

callbacks_list = [checkpoint, lr_callback]


optim = keras.optimizers.Adam()# lr=8e-5) # clipnorm = 1.0 ,amsgrad=True
model.compile(optim , loss='mean_absolute_error')
history = model.fit(dataset_train, epochs=3000,callbacks=callbacks_list,validation_data= dataset_valid); 

str_loss = "./Experiments/Results/Loss.mat"
sio.savemat(str_loss, {'loss':history.history['loss'],'val_loss':history.history['val_loss']})

"""# 4) Generate the dictionary"""

nTotal = 7812
nBatch = 7812
model_valid = build_model_GRU_init(
    rnn_units = nRNN_Unit,
    input_batch = nBatch,
    input_size = nInput, # RF, T1, T2, don't put in mxy(n-1)
    output_size = nOutput)

str1 = "./Experiments/Results/RNN_EPG.hdf5"  
model_valid.load_weights(str1)

B1 = np.linspace(1.0,1.2,1) 


Data_Dic = np.zeros(shape=(nTotal * len(B1), nSeq_Length, nInput),dtype="float32")
State_Input_Dic = np.zeros(shape=(nTotal * len(B1), 3),dtype="float32")
State_Input_Dic[:,2] = -1.0
print(State_Input_Dic.shape)

str1 = "./Experiments/MRF_Dictionary/flipangle.mat"

traindata = sio.loadmat(str1)
RFtrain = traindata.get("flipangles") # flipangles
TE = traindata.get("TE")
TR = traindata.get("TR")

str1 = "./Experiments/MRF_Dictionary/Dictionary_EPG.mat"
traindata = sio.loadmat(str1)
Dictionary = traindata.get("Dictionary") # flipangles
T1T2B1 = traindata.get("T1T2B1")
print(T1T2B1.shape)
T1 = T1T2B1[:,0]  
T1 = T1[:,np.newaxis]
T2 = T1T2B1[:,1]
T2 = T2[:,np.newaxis]

for ii in range(len(B1)):
  print(ii)
  Data_Dic[ii * nTotal:(ii+1) * nTotal,0:nSeq_Length,0] = np.squeeze(RFtrain) * B1[ii] # RF
  Data_Dic[ii * nTotal:(ii+1) * nTotal,:,1] = T1          # T1
  Data_Dic[ii * nTotal:(ii+1) * nTotal,:,2] = T2          # T2
  Data_Dic[ii * nTotal:(ii+1) * nTotal,:,3] = 0.0049226   # TE
  Data_Dic[ii * nTotal:(ii+1) * nTotal,:,4] = 0.0087552   # TR
dataset_valid = tf.data.Dataset.from_tensor_slices(({"input_1": Data_Dic, "input_2": State_Input_Dic}))
dataset_valid = dataset_valid.batch(nBatch, drop_remainder=True) # nTotal

# compute y_predict
tic = time.time()
y_predict = model_valid.predict(dataset_valid) # model.predict(x) # x is tensor, but y_predict is numpy
toc = time.time()
print("Predict Time is:",toc-tic)

sio.savemat('./Experiments/MRF_Dictionary/Dictionary_RNN.mat', {'Dictionary':y_predict, 'T1T2B1':T1T2B1})
